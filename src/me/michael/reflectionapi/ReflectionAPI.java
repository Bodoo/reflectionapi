/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi;

import java.util.logging.Level;
import java.util.logging.Logger;
import me.michael.reflectionapi.wrapper.WrappedClass;
import me.michael.reflectionapi.wrapper.WrappedObject;
import me.obscurepath.Main;

/**
 *
 * @author Michael
 */
public class ReflectionAPI {

    public static void main(String[] args) throws Exception {
        ReflectionAPI.obtainClass("me.michael.somepathinanotherjar.GameState").getEnum("STARTING").getDeclaredUnknown("getStateInfo").getDeclaredMethod("printToConsole").invoke();
    }
    
    public static WrappedClass obtainClass(String classPath) throws ClassNotFoundException{
        Class clazz = Class.forName(classPath);
        return new WrappedClass(clazz);
    }
    
    public static WrappedClass obtainClass(Class clazz) throws ClassNotFoundException{
        return new WrappedClass(clazz);
    }
    
    public static WrappedObject obtainObject(Object object){
        return new WrappedObject(object);
    }
    
    public static String toString(Object object){
        try {
            WrappedObject obj = ReflectionAPI.obtainObject(object);
            return obj.toReflectiveString();
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException ex) {
            Logger.getLogger(ReflectionAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
