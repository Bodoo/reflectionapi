/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.michael.reflectionapi;

public class ReflectionUtil {

    public static Class[] parameterTypesByObjects(Object... objects){
        Class[] clazzarray = new Class[objects.length];
        for(int i = 0; i < objects.length; i++){
            clazzarray[i] = objects[i].getClass();
        }
        return clazzarray;
    }
    
}
