/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.michael.reflectionapi.wrapper;

import me.michael.reflectionapi.wrapper.abstr.IEnumHolder;
import me.michael.reflectionapi.wrapper.abstr.IFieldHolder;
import me.michael.reflectionapi.wrapper.abstr.IMethodHolder;
import me.michael.reflectionapi.wrapper.abstr.IUnknownHolder;

public abstract class WrappedEntity implements IMethodHolder, IUnknownHolder, IFieldHolder, IEnumHolder{

    protected boolean isInstantiated() { return getEntityObject() != null; } ;
    
    protected Class getEntityClass() { return null; } ;
    
    protected Object getEntityObject() { return null; } ;
    
}
