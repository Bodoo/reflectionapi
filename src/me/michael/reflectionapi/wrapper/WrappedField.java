/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper;

import java.lang.reflect.Field;

public class WrappedField {

    private WrappedEntity wrappedEntity;
    private String fieldName;
    private boolean setAccessible;
    private boolean declared;

    private Field field;

    public WrappedField(WrappedEntity wrappedEntity, String fieldName, boolean setAccessible, boolean declared) throws NoSuchFieldException {
        this.wrappedEntity = wrappedEntity;
        this.fieldName = fieldName;
        this.setAccessible = setAccessible;
        this.declared = declared;
    }

    private void initializeField() throws NoSuchFieldException {
        if (field == null) {
            if (declared) {
                if (wrappedEntity.isInstantiated()) {
                    if (declared) {
                        field = wrappedEntity.getEntityObject().getClass().getDeclaredField(fieldName);
                    } else {
                        field = wrappedEntity.getEntityObject().getClass().getField(fieldName);
                    }
                } else if (declared) {
                    field = wrappedEntity.getEntityClass().getDeclaredField(fieldName);
                } else {
                    field = wrappedEntity.getEntityClass().getField(fieldName);
                }
            }
            if (setAccessible) {
                field.setAccessible(true);
            }
        }
    }
    
    public String getName(){
        return fieldName;
    }

    public Field getField() throws NoSuchFieldException {
        initializeField();
        return field;
    }
    
    public void set(Object value) throws IllegalArgumentException, NoSuchFieldException, IllegalAccessException{
        initializeField();
        field.set(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, value);
    }

    public Object get() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        initializeField();
        return field.get(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null);
    }
    
    public <T> T get(Class<T> clazz) throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        initializeField();
        return (T) field.get(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null);
    }

}
