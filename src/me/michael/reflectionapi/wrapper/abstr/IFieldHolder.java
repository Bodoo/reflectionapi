/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper.abstr;

import java.util.List;
import me.michael.reflectionapi.wrapper.WrappedField;

public interface IFieldHolder {

    default WrappedField getDeclaredField(String name) throws NoSuchFieldException{ return getDeclaredField(name, true); } ;

    default WrappedField getField(String name) throws NoSuchFieldException { return getField(name, true); } ;
    
    public WrappedField getDeclaredField(String name, boolean setAccessible) throws NoSuchFieldException;

    public WrappedField getField(String name, boolean setAccessible) throws NoSuchFieldException;

    default List<WrappedField> getFields() throws NoSuchFieldException { return getFields(false, true); };
    
    public List<WrappedField> getFields(boolean declared, boolean setAccessible) throws NoSuchFieldException;
    
}
