/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper.abstr;

import java.util.List;
import me.michael.reflectionapi.wrapper.WrappedEnum;

public interface IEnumHolder {

    public WrappedEnum getEnum(String enumName);
    
    public List<WrappedEnum> enumValues();
    
}
