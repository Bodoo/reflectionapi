/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper.abstr;

import me.michael.reflectionapi.wrapper.WrappedMethod;
import me.michael.reflectionapi.wrapper.WrappedMethod;
import me.michael.reflectionapi.wrapper.WrappedVoidMethod;
import me.michael.reflectionapi.wrapper.WrappedVoidMethod;

public interface IMethodHolder {
    
    public WrappedVoidMethod getDeclaredMethod(String name, Class... parameterTypes);
    
    public WrappedVoidMethod getMethod(String name, Class... parameterTypes);
    
    
    public <T> WrappedMethod<T> getDeclaredMethod(Class<T> returnType, String name, Class... parameterTypes);
    
    public <T> WrappedMethod<T> getMethod(Class<T> returnType, String name, Class... parameterTypes);
    
    
}
