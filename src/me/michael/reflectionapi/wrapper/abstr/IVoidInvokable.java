/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper.abstr;

import java.lang.reflect.InvocationTargetException;

public interface IVoidInvokable {
     
    public void invoke() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException;
    
    public void invoke(Object... arguments) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException;
    
}
