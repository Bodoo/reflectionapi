/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper.abstr;

import java.lang.reflect.InvocationTargetException;
import me.michael.reflectionapi.wrapper.WrappedUnknown;

public interface IUnknownHolder {
    
    public WrappedUnknown getDeclaredUnknown(String methodName, Object... arguments) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException ;
    
    public WrappedUnknown getUnknown(String methodName, Object... arguments) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException ;
    
}
