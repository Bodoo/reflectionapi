/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import me.michael.reflectionapi.ReflectionUtil;

public class WrappedUnknown extends WrappedEntity {

    private WrappedEntity wrappedEntity;
    private String methodName;
    private Class[] parameterTypes;
    private Object[] arguments;

    private boolean declared;

    private Object unknownObject;

    public Object getObject(){
        return unknownObject;
    }
    
    public <T> T as(Class<T> type){
        return (T) unknownObject;
    }
    
    @Override
    protected boolean isInstantiated() {
        return true;
    }

    @Override
    protected Object getEntityObject() {
        return unknownObject;
    }
    public WrappedUnknown(WrappedEntity wrappedEntity, String methodName, Object[] arguments, boolean declared) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        this.wrappedEntity = wrappedEntity;
        this.methodName = methodName;
        this.arguments = arguments;
        this.declared = declared;

        this.parameterTypes = ReflectionUtil.parameterTypesByObjects(arguments);
        if (declared) {
            Object[] arg = arguments == null ? null : arguments.length == 0 ? new Object[0] : arguments.length == 1 && arguments[0] == null ? new Object[0] : arguments;
            if (wrappedEntity.isInstantiated()) {
                if (declared) {
                    unknownObject = wrappedEntity.getEntityObject().getClass().getDeclaredMethod(methodName, parameterTypes).invoke(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, arg);
                } else {
                    unknownObject = wrappedEntity.getEntityObject().getClass().getMethod(methodName, parameterTypes).invoke(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, arg);
                }
            } else {
                if (declared) {
                    unknownObject = wrappedEntity.getEntityClass().getDeclaredMethod(methodName, parameterTypes).invoke(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, arg);
                } else {
                    unknownObject = wrappedEntity.getEntityClass().getMethod(methodName, parameterTypes).invoke(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, arg);
                }
            }
        }
    }

    @Override
    public WrappedVoidMethod getDeclaredMethod(String methodName, Class... parameterTypes) {
        return new WrappedVoidMethod(this, methodName, parameterTypes, true);
    }

    @Override
    public WrappedVoidMethod getMethod(String methodName, Class... parameterTypes) {
        return new WrappedVoidMethod(this, methodName, parameterTypes, false);
    }

    @Override
    public <T> WrappedMethod<T> getDeclaredMethod(Class<T> returnType, String methodName, Class... parameterTypes) {
        return new WrappedMethod(this, methodName, parameterTypes, true);
    }

    @Override
    public <T> WrappedMethod<T> getMethod(Class<T> returnType, String methodName, Class... parameterTypes) {
        return new WrappedMethod(this, methodName, parameterTypes, false);
    }

    @Override
    public WrappedUnknown getDeclaredUnknown(String methodName, Object... arguments) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return new WrappedUnknown(this, methodName, arguments, true);
    }

    @Override
    public WrappedUnknown getUnknown(String methodName, Object... arguments) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return new WrappedUnknown(this, methodName, arguments, false);
    }

    @Override
    public WrappedField getDeclaredField(String name, boolean setAccessible) throws NoSuchFieldException {
        return new WrappedField(this, name, setAccessible, true);
    }

    @Override
    public WrappedField getField(String name, boolean setAccessible) throws NoSuchFieldException {
        return new WrappedField(this, name, setAccessible, false);
    }

    @Override
    public List<WrappedField> getFields(boolean declared, boolean setAccessible) throws NoSuchFieldException{
        List<WrappedField> list = new ArrayList<>();
        for(Field f : declared ? unknownObject.getClass().getDeclaredFields() : unknownObject.getClass().getFields()){
            WrappedField wrappedField = new WrappedField(this, f.getName(), setAccessible, declared);
            list.add(wrappedField);
        }
        return list;
    }
    
    @Override
    public WrappedEnum getEnum(String enumName) {
        return enumValues().stream().filter((e)->e.name().equals(enumName)).findFirst().orElse(null);
    }

    @Override
    public List<WrappedEnum> enumValues() {
        Object[] enumConstants = unknownObject.getClass().getEnumConstants();
        if(enumConstants == null){
            return null;
        }
        List<WrappedEnum> list = new ArrayList();
        for(Object enumC : enumConstants){
            list.add(new WrappedEnum(this, enumC));
        }
        return list;
    }
    
}
