/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper;

import java.lang.reflect.InvocationTargetException;
import me.michael.reflectionapi.wrapper.abstr.IInvokable;

public class WrappedMethod<T> implements IInvokable<T> {

    private WrappedEntity wrappedEntity;
    private String methodName;
    private Class[] parameterTypes;

    private boolean declared;

    public WrappedMethod(WrappedEntity wrappedEntity, String methodName, Class[] parameterTypes, boolean declared) {
        this.wrappedEntity = wrappedEntity;
        this.methodName = methodName;
        this.parameterTypes = parameterTypes;
        this.declared = declared;
    }

    @Override
    public T invoke() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        return invoke(null);
    }

    @Override
    public T invoke(Object... arguments) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Object[] arg = arguments == null ? null : arguments.length == 0 ? new Object[0] : arguments.length == 1 && arguments[0] == null ? new Object[0] : arguments;
        if (wrappedEntity.isInstantiated()) {
            if (declared) {
                return (T) wrappedEntity.getEntityObject().getClass().getDeclaredMethod(methodName, parameterTypes).invoke(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, arg);
            } else {
                return (T) wrappedEntity.getEntityObject().getClass().getMethod(methodName, parameterTypes).invoke(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, arg);
            }
        } else {
            if (declared) {
                return (T) wrappedEntity.getEntityClass().getDeclaredMethod(methodName, parameterTypes).invoke(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, arg);
            } else {
                return (T) wrappedEntity.getEntityClass().getMethod(methodName, parameterTypes).invoke(wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject() : null, arg);
            }
        }
    }

}
