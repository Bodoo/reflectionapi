/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.michael.reflectionapi.wrapper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class WrappedClass extends WrappedEntity {

    private Class clazz;

    public WrappedClass(Class clazz) {
        this.clazz = clazz;
    }

    @Override
    protected Class getEntityClass() {
        return clazz;
    }

    @Override
    public WrappedVoidMethod getDeclaredMethod(String methodName, Class... parameterTypes) {
        return new WrappedVoidMethod(this, methodName, parameterTypes, true);
    }

    @Override
    public WrappedVoidMethod getMethod(String methodName, Class... parameterTypes) {
        return new WrappedVoidMethod(this, methodName, parameterTypes, false);
    }

    @Override
    public <T> WrappedMethod<T> getDeclaredMethod(Class<T> returnType, String methodName, Class... parameterTypes) {
        return new WrappedMethod(this, methodName, parameterTypes, true);
    }

    @Override
    public <T> WrappedMethod<T> getMethod(Class<T> returnType, String methodName, Class... parameterTypes) {
        return new WrappedMethod(this, methodName, parameterTypes, false);
    }

    @Override
    public WrappedUnknown getDeclaredUnknown(String methodName, Object... arguments) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return new WrappedUnknown(this, methodName, arguments, true);
    }

    @Override
    public WrappedUnknown getUnknown(String methodName, Object... arguments) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return new WrappedUnknown(this, methodName, arguments, false);
    }

    @Override
    public WrappedField getDeclaredField(String name, boolean setAccessible) throws NoSuchFieldException {
        return new WrappedField(this, name, setAccessible, true);
    }

    @Override
    public WrappedField getField(String name, boolean setAccessible) throws NoSuchFieldException {
        return new WrappedField(this, name, setAccessible, false);
    }

    @Override
    public List<WrappedField> getFields(boolean declared, boolean setAccessible) throws NoSuchFieldException{
        List<WrappedField> list = new ArrayList<>();
        for(Field f : declared ? clazz.getDeclaredFields() : clazz.getFields()){
            WrappedField wrappedField = new WrappedField(this, f.getName(), setAccessible, declared);
            list.add(wrappedField);
        }
        return list;
    }

    @Override
    public WrappedEnum getEnum(String enumName) {
        return enumValues().stream().filter((e)->e.name().equals(enumName)).findFirst().orElse(null);
    }

    @Override
    public List<WrappedEnum> enumValues() {
        Object[] enumConstants = getEntityClass().getEnumConstants();
        if(enumConstants == null){
            return null;
        }
        List<WrappedEnum> list = new ArrayList();
        for(Object enumC : enumConstants){
            list.add(new WrappedEnum(this, enumC));
        }
        return list;
    }

    
}
