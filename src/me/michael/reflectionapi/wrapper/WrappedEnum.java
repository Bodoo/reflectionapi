/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.michael.reflectionapi.wrapper;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import me.michael.reflectionapi.ReflectionAPI;

public class WrappedEnum extends WrappedEntity{

    private WrappedEntity wrappedEntity;
    private Object enumConstant;
    
    public WrappedEnum(WrappedEntity wrappedEntity, Object enumConstant){
        this.wrappedEntity = wrappedEntity;
        this.enumConstant = enumConstant;
    }
    
    public String name(){
        try {
            return ReflectionAPI.obtainObject(enumConstant).getMethod(String.class, "name").invoke();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(WrappedEnum.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int ordinal(){
        try {
            return ReflectionAPI.obtainObject(enumConstant).getMethod(Integer.TYPE, "ordinal").invoke();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ex) {
            Logger.getLogger(WrappedEnum.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    public <T> T as(Class<T> clazz){
        return (T) enumConstant;
    }

@Override
    public WrappedVoidMethod getDeclaredMethod(String methodName, Class... parameterTypes) {
        return new WrappedVoidMethod(ReflectionAPI.obtainObject(enumConstant), methodName, parameterTypes, true);
    }

    @Override
    public WrappedVoidMethod getMethod(String methodName, Class... parameterTypes) {
        return new WrappedVoidMethod(ReflectionAPI.obtainObject(enumConstant), methodName, parameterTypes, false);
    }

    @Override
    public <T> WrappedMethod<T> getDeclaredMethod(Class<T> returnType, String methodName, Class... parameterTypes) {
        return new WrappedMethod(ReflectionAPI.obtainObject(enumConstant), methodName, parameterTypes, true);
    }

    @Override
    public <T> WrappedMethod<T> getMethod(Class<T> returnType, String methodName, Class... parameterTypes) {
        return new WrappedMethod(ReflectionAPI.obtainObject(enumConstant), methodName, parameterTypes, false);
    }

    @Override
    public WrappedUnknown getDeclaredUnknown(String methodName, Object... arguments) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return new WrappedUnknown(ReflectionAPI.obtainObject(enumConstant), methodName, arguments, true);
    }

    @Override
    public WrappedUnknown getUnknown(String methodName, Object... arguments) throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return new WrappedUnknown(ReflectionAPI.obtainObject(enumConstant), methodName, arguments, false);
    }

    @Override
    public WrappedField getDeclaredField(String name, boolean setAccessible) throws NoSuchFieldException {
        return new WrappedField(ReflectionAPI.obtainObject(enumConstant), name, setAccessible, true);
    }

    @Override
    public WrappedField getField(String name, boolean setAccessible) throws NoSuchFieldException {
        return new WrappedField(ReflectionAPI.obtainObject(enumConstant), name, setAccessible, false);
    }
    
    @Override
    public List<WrappedField> getFields(boolean declared, boolean setAccessible) throws NoSuchFieldException{
        List<WrappedField> list = new ArrayList<>();
        for(Field f : declared ? this.getClass().getDeclaredFields(): enumConstant.getClass().getFields()){
            WrappedField wrappedField = new WrappedField(ReflectionAPI.obtainObject(enumConstant), f.getName(), setAccessible, declared);
            list.add(wrappedField);
        }
        return list;
    }

    @Override
    public WrappedEnum getEnum(String enumName) {
        return enumValues().stream().filter((e)->e.name().equals(enumName)).findFirst().orElse(null);
    }

    @Override
    public List<WrappedEnum> enumValues() {
        Object[] enumConstants = wrappedEntity.isInstantiated() ? wrappedEntity.getEntityObject().getClass().getEnumConstants() : wrappedEntity.getEntityClass().getEnumConstants();
        if(enumConstants == null){
            return null;
        }
        List<WrappedEnum> list = new ArrayList();
        for(Object enumC : enumConstants){
            list.add(new WrappedEnum(this, enumC));
        }
        return list;
    }
    
}
